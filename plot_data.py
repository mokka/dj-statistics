import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import csv
import sys

def scale_y_axis():
    axes = plt.gca()
    _, y_top = axes.get_ylim()
    y_top += y_top * 0.1
    axes.set_ylim([0,y_top])

def div(x,y):
    return x / y

dates = []
members_jam = []
submissions = []
members_discord = []

audio = []
artists = []
programmers = []
writers = []
moderators = []

english = []
french = []
spanish = []
german = []

roleplayers = []
without_role = []


with open('decade_jam_stats.csv', 'r') as csvfile:
    plots = csv.reader(filter(lambda row: row[0]!='#', csvfile), delimiter=',')
    for row in plots:
        dates.append(row[0])
        members_jam.append(int(row[1]))
        submissions.append(int(row[2]))
        members_discord.append(int(row[3]))
        audio.append(int(row[4]))
        artists.append(int(row[5]))
        programmers.append(int(row[6]))
        writers.append(int(row[7]))
        moderators.append(int(row[8]))
        english.append(int(row[9]))
        french.append(int(row[10]))
        spanish.append(int(row[11]))
        german.append(int(row[12]))
        roleplayers.append(int(row[13]))
        without_role.append(int(row[14]))

audio = np.array(list(map(div, audio, members_discord)))
artists = np.array(list(map(div, artists, members_discord)))
programmers = np.array(list(map(div, programmers, members_discord)))
writers = np.array(list(map(div, writers, members_discord)))
moderators = np.array(list(map(div, moderators, members_discord)))
english = np.array(list(map(div, english, members_discord)))
french = np.array(list(map(div, french, members_discord)))
spanish = np.array(list(map(div, spanish, members_discord)))
german = np.array(list(map(div, german, members_discord)))
roleplayers = np.array(list(map(div, roleplayers, members_discord)))
without_role = np.array(list(map(div, without_role, members_discord)))

plt.figure(1)

plt.plot_date(dates, members_jam, label='Members (Jam)', ls='solid')
plt.plot_date(dates, members_discord, label='Members (Discord)', ls='solid')
plt.xlabel('Date')
plt.ylabel('Members')
plt.title('Members (Jam & Discord)')

scale_y_axis()

plt.legend()

plt.savefig('out/members.png')

plt.figure(2)

plt.plot_date(dates, submissions, label='Submissions', ls='solid')
plt.xlabel('Date')
plt.ylabel('Submissions')
plt.title('Submissions')

scale_y_axis()

plt.legend()

plt.savefig('out/submissions.png')

plt.figure(3)

plt.plot_date(dates, audio, label='Audio Folks', ls='solid', c='b')
plt.plot_date(dates, artists, label='Artists', ls='solid', c='m')
plt.plot_date(dates, programmers, label='Programmers', ls='solid', c='c')
plt.plot_date(dates, writers, label='Writers', ls='solid', c='g')
plt.plot_date(dates, moderators, label='Moderators', ls='solid', c='y')
plt.plot_date(dates, roleplayers, label='Roleplayers', ls='solid', c='r')
plt.plot_date(dates, without_role, label='Without A Role', ls='solid', c='grey')

plt.xlabel('Date')
plt.ylabel('Members with role (rel. to total member count)')
plt.title('Role Distribution')

plt.gca().set_ylim([0,1])

plt.legend()

plt.savefig('out/roles.png')

plt.figure(4)

plt.plot_date(dates, english, label='English', ls='solid')
plt.plot_date(dates, french, label='French', ls='solid')
plt.plot_date(dates, spanish, label='Spanish', ls='solid')
plt.plot_date(dates, german, label='German', ls='solid')

plt.xlabel('Date')
plt.ylabel('Speakers (rle. to total member count)')
plt.title('Most Common Languages')

scale_y_axis()

plt.legend()

if '--show' in sys.argv:
    plt.show()

plt.savefig('out/languages.png')